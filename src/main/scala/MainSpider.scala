import akka.actor._
import akka.io.IO
import akka.util.Timeout
import spray.can.Http
import spray.http._
import akka.pattern.ask
import spray.http.parser.{ParserInput, HttpParser}
import scala.concurrent.duration._
import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits._

object SearchActor {

  case class InitSuperior(url: String, depth: Int, output: Option[Int])

  case class InitWorker(url: String, depth: Int)

  case class Resp(links: List[String])

}

class SearchWorkerActor extends Actor {

  import SearchActor._

  def receive: Receive = {

    case InitWorker(url: String, depth: Int) => {

      implicit val timeout = Timeout(25.seconds)
      val senderThis = sender()
      try {
        val uri = Uri(ParserInput.apply(url), HttpCharsets.`UTF-8`.nioCharset, Uri.ParsingMode.RelaxedWithRawQuery)
        val response2 =
          (IO(Http)(context.system) ? HttpRequest(HttpMethods.GET, uri)).mapTo[HttpResponse]

        response2.onComplete {
          case Success(response) => {

            // val regex = "<a href=\"[(http://)(https://)/]+([^\"]*)\".*".r
            val regex = "<a href=\"([^\"]*)\"".r
            val html = response.entity.asString

            val myResp = regex.findAllIn(html).matchData.map(x => x.group(1)).map(
              x => (resolveLocalRefs(x, url),
              try {
                Option(Uri(ParserInput.apply(resolveLocalRefs(x, url)), HttpCharsets.`UTF-8`.nioCharset, Uri.ParsingMode.RelaxedWithRawQuery).authority.host)
              }
              catch {
                case e: Exception => println(e.getMessage); None;
              }
            )).toSet

            //<a href="http://poczta.wp.pl">Poczta</a>

            if (depth == 0) {

              val tmp = myResp.filter(x => x._2 != None).map(x => x._2.get.toString).toList
              senderThis ! Resp(tmp)
            } else {
              val all_subWorkers = (for {x <- myResp; actor = context.actorOf(Props[SearchWorkerActor])} yield (actor, x)).toSet
              //println(all_subWorkers.size)
              all_subWorkers.foreach(x => x._1 ! InitWorker(x._2._1, depth - 1))

              context.become(inWorkWorker(all_subWorkers.map(x => x._1), myResp.filter(x => x._2 != None).map(x => x._2.get.toString).toList, senderThis))
            }
          }

          case Failure(t) => senderThis ! Resp(List())
        }
      } catch {
        case e: Exception =>
          senderThis ! Resp(List())
      }

    }

  }

  def resolveLocalRefs(x: String, url: String): String = {
    (if (x.contains("http://") || x.contains("https://")) x else url + (if ((url take (url.length - 1)) != "/") "/" else "") + x)
  }

  def inWorkWorker(subworkers: Set[ActorRef], myRespAccumulator: List[String], senderThis: ActorRef): Receive = {

    case Resp(list: List[String]) => {
      if (subworkers.size > 1) {

        // context.become(inWorkWorker(subworkers.filterNot( x => x.equals(sender())), myRespAccumulator ++ list ))
        sender() ! PoisonPill
        context.become(inWorkWorker((subworkers - sender()), myRespAccumulator ++ list, senderThis))
      } else {

        senderThis ! Resp(myRespAccumulator ++ list)
      }
    }
  }
}

class SearchSuperiorActor extends Actor {

  import SearchActor._

  implicit val timeout = Timeout(150.seconds)

  def receive: Receive = {

    case InitSuperior(url, depth, output) =>

      val worker = context.actorOf(Props[SearchWorkerActor], "worker")

      val future = ask(worker, InitWorker(url, depth)).mapTo[Resp]

      future.onComplete {

        case Success(Resp(hosts)) =>
          //mkString( System.lineSeparator() )
          //println(links)

          computeList(hosts).take(output.getOrElse(hosts.length)).foreach(x => println("ilość wystapień: " + x._2 + "      host: " + x._1))
          context.system.shutdown()


        case Failure(_) =>
          println("Search error, shutting down" )
          context.system.shutdown()

        case _ => println("czemu")
      }

  }

  def computeList(list: List[String]): List[(String, Int)] = {
    list.groupBy(x => x).map(x => (x._1, x._2.length)).toList.sortBy(x => x._2).reverse
  }
}


object MainSpider {

  import SearchActor._

  def main(args: Array[String]) {

    val sys = ActorSystem("system")
    val main = sys.actorOf(Props[SearchSuperiorActor], "MainActor")

    if (args.length == 2) {
      main ! InitSuperior(args(0), args(1).toInt, None)
    } else if (args.length == 3) {
      main ! InitSuperior(args(0), args(1).toInt, Some(args(2).toInt))
    } else {
      println("podaj adres url i glebokosc, opcjonalnie ilosc wyswitlanych linkow")
      return
    }

  }
}



